from setuptools import setup 

setup(name='geo-shapely',
      description='addons to shapely to handle geographic objects',
      url='https://gitlab.ifremer.fr/oa04eb3/geo_shapely.git',
      author = "Olivier Archer",
      author_email = "Olivier.Archer@ifremer.fr",
      license='GPL',
      packages=['geo_shapely'],
      use_scm_version=True,
      setup_requires=['setuptools_scm'],
      zip_safe=False,
      install_requires=[ 'shapely','numpy','pyproj' ]
)
